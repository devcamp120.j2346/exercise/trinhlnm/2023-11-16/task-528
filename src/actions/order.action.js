import { BUTTON_XOA_CLICKED, CLEAR_MESSAGE, CLEAR_ORDER_DATA, CREATE_DON_ERROR, CREATE_DON_PENDING, CREATE_DON_SUCCESS, DELETE_DON_ERROR, DELETE_DON_PENDING, DELETE_DON_SUCCESS, EDIT_DON_ERROR, EDIT_DON_PENDING, EDIT_DON_SUCCESS, INPUT_DIA_CHI_CHANGE, INPUT_EMAIL_CHANGE, INPUT_HOTEN_CHANGE, INPUT_LOI_NHAN_CHANGE, INPUT_SDT_CHANGE, INPUT_VOUCHER_ID_CHANGE, ON_CLOSE_MODAL, ORDER_BY_ID_FETCH_ERROR, ORDER_BY_ID_FETCH_PENDING, ORDER_BY_ID_FETCH_SUCCESS, ORDER_FETCH_ERROR, ORDER_FETCH_PENDING, ORDER_FETCH_SUCCESS, SELECT_DRINK_CHANGE, SELECT_SIZE_CHANGE, SELECT_STATUS_CHANGE, SELECT_TYPE_CHANGE, VOUCHER_FETCH_ERROR, VOUCHER_FETCH_PENDING, VOUCHER_FETCH_SUCCESS } from "../constants/order.constant";

export const fetchOrder = () => {
    return async (dispatch) => {
        try {
            var requestOptions = {
                method: "GET"
            }

            await dispatch({
                type: ORDER_FETCH_PENDING
            });

            const resOrders = await fetch("http://203.171.20.210:8080/devcamp-pizza365/orders", requestOptions);

            const dataOrders = await resOrders.json();

            return dispatch({
                type: ORDER_FETCH_SUCCESS,
                data: dataOrders
            });
        } catch (error) {
            return dispatch({
                type: ORDER_FETCH_ERROR,
                error: error
            });
        }
    }
}

export const selectSizeChangeAction = (selectValue) => {
    return {
        type: SELECT_SIZE_CHANGE,
        payload: selectValue
    }
}

export const selectTypeChangeAction = (selectValue) => {
    return {
        type: SELECT_TYPE_CHANGE,
        payload: selectValue
    }
}

export const inputVoucherIDChangeAction = (inputValue) => {
    return {
        type: INPUT_VOUCHER_ID_CHANGE,
        payload: inputValue
    }
}

export const btnCheckVoucherClickedAction = (inputValue) => {
    return async (dispatch) => {
        try {
            var requestOptions = {
                method: "GET"
            }

            await dispatch({
                type: VOUCHER_FETCH_PENDING
            });

            const resPhanTramVoucher = await fetch("http://203.171.20.210:8080/devcamp-pizza365/vouchers/" + inputValue, requestOptions);

            const dataPhanTramVoucher = await resPhanTramVoucher.json();

            return dispatch({
                type: VOUCHER_FETCH_SUCCESS,
                data: dataPhanTramVoucher.discount
            });
        } catch (error) {
            return dispatch({
                type: VOUCHER_FETCH_ERROR,
                error: error
            });
        }
    }
}

export const selectDrinkChangeAction = (selectValue) => {
    return {
        type: SELECT_DRINK_CHANGE,
        payload: selectValue
    }
}

export const inputHotenChangeAction = (inputValue) => {
    return {
        type: INPUT_HOTEN_CHANGE,
        payload: inputValue
    }
}

export const inputEmailChangeAction = (inputValue) => {
    return {
        type: INPUT_EMAIL_CHANGE,
        payload: inputValue
    }
}

export const inputSdtChangeAction = (inputValue) => {
    return {
        type: INPUT_SDT_CHANGE,
        payload: inputValue
    }
}

export const inputDiaChiChangeAction = (inputValue) => {
    return {
        type: INPUT_DIA_CHI_CHANGE,
        payload: inputValue
    }
}

export const inputLoiNhanChangeAction = (inputValue) => {
    return {
        type: INPUT_LOI_NHAN_CHANGE,
        payload: inputValue
    }
}

export const btnTaoDonHangClickedAction = (
    selectSizeValue, duongKinh, suon, salad,
    selectTypeValue, inputVoucherId, idLoaiNuocUong, soLuongNuoc,
    hoTen, thanhTien, email, soDienThoai, diaChi, loiNhan
) => {
    return async (dispatch) => {
        try {
            var vObjectRequest = {
                kichCo: selectSizeValue,
                duongKinh: duongKinh,
                suon: suon,
                salad: salad,
                loaiPizza: selectTypeValue,
                idVourcher: inputVoucherId,
                idLoaiNuocUong: idLoaiNuocUong,
                soLuongNuoc: soLuongNuoc,
                hoTen: hoTen,
                thanhTien: thanhTien,
                email: email,
                soDienThoai: soDienThoai,
                diaChi: diaChi,
                loiNhan: loiNhan
            }    

            var requestOptions = {
                method: "POST",
                body: JSON.stringify(vObjectRequest),
                headers: {
                    'Content-type': 'application/json;charset=UTF-8',
                },
            }

            await dispatch({
                type: CREATE_DON_PENDING
            });

            const resCreateOrder = await fetch("http://203.171.20.210:8080/devcamp-pizza365/orders", requestOptions);

            const dataCreateOrder = await resCreateOrder.json();

            return dispatch({
                type: CREATE_DON_SUCCESS,
                data: dataCreateOrder
            });
        } catch (error) {
            return dispatch({
                type: CREATE_DON_ERROR,
                error: error
            });
        }
    }
}

export const clearOrderDataAction = () => {
    return {
        type: CLEAR_ORDER_DATA
    };
}

export const fetchOrderById = (id) => {
    return async (dispatch) => {
        try {
            var requestOptions = {
                method: "GET"
            }

            await dispatch({
                type: ORDER_BY_ID_FETCH_PENDING
            });

            const resOrderById = await fetch("http://203.171.20.210:8080/devcamp-pizza365/orders" + "/" + id, requestOptions);

            const dataOrderById = await resOrderById.json();

            return dispatch({
                type: ORDER_BY_ID_FETCH_SUCCESS,
                data: dataOrderById
            });
        } catch (error) {
            return dispatch({
                type: ORDER_BY_ID_FETCH_ERROR,
                error: error
            });
        }
    }
}

export const selectStatusChangeAction = (selectValue) => {
    return {
        type: SELECT_STATUS_CHANGE,
        payload: selectValue
    }
}

export const btnSuaDonHangClickedAction = (trangThai, id) => {
    return async (dispatch) => {
        try {
            var vObjectRequest = {
                trangThai: trangThai
            }    

            var requestOptions = {
                method: "PUT",
                body: JSON.stringify(vObjectRequest),
                headers: {
                    'Content-type': 'application/json;charset=UTF-8',
                },
            }

            await dispatch({
                type: EDIT_DON_PENDING
            });

            const resEditOrder = await fetch("http://203.171.20.210:8080/devcamp-pizza365/orders" + "/" + id, requestOptions);

            const dataEditOrder = await resEditOrder.json();

            return dispatch({
                type: EDIT_DON_SUCCESS,
                data: dataEditOrder
            });
        } catch (error) {
            return dispatch({
                type: EDIT_DON_ERROR,
                error: error
            });
        }
    }
}

export const clearMessageAction = () => {
    return {
        type: CLEAR_MESSAGE
    };
}

export const btnXoaClickedAction = (id) => {
    return {
        type: BUTTON_XOA_CLICKED,
        payload: id
    };
}

export const onModalClose = () => {
    return {
        type: ON_CLOSE_MODAL
    };
}

export const btnXacNhanXoaClickedAction = (id) => {
    return async (dispatch) => {
        try {
            var requestOptions = {
                method: "DELETE"
            }

            await dispatch({
                type: DELETE_DON_PENDING
            });

            const resDeleteOrder = await fetch("http://203.171.20.210:8080/devcamp-pizza365/orders" + "/" + id, requestOptions);

            return dispatch({
                type: DELETE_DON_SUCCESS,
                data: resDeleteOrder
            });
        } catch (error) {
            return dispatch({
                type: DELETE_DON_ERROR,
                error: error
            });
        }
    }
}