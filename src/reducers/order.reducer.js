import { BUTTON_XOA_CLICKED, CLEAR_MESSAGE, CLEAR_ORDER_DATA, CREATE_DON_ERROR, CREATE_DON_PENDING, CREATE_DON_SUCCESS, DELETE_DON_ERROR, DELETE_DON_PENDING, DELETE_DON_SUCCESS, EDIT_DON_ERROR, EDIT_DON_PENDING, EDIT_DON_SUCCESS, INPUT_DIA_CHI_CHANGE, INPUT_EMAIL_CHANGE, INPUT_HOTEN_CHANGE, INPUT_LOI_NHAN_CHANGE, INPUT_SDT_CHANGE, INPUT_VOUCHER_ID_CHANGE, ON_CLOSE_MODAL, ORDER_BY_ID_FETCH_ERROR, ORDER_BY_ID_FETCH_PENDING, ORDER_BY_ID_FETCH_SUCCESS, ORDER_FETCH_ERROR, ORDER_FETCH_PENDING, ORDER_FETCH_SUCCESS, SELECT_DRINK_CHANGE, SELECT_SIZE_CHANGE, SELECT_STATUS_CHANGE, SELECT_TYPE_CHANGE, VOUCHER_FETCH_ERROR, VOUCHER_FETCH_PENDING, VOUCHER_FETCH_SUCCESS } from "../constants/order.constant";

const initialState = {
    orders: [],
    pending: false,
    selectSizeValue: "",
    duongKinh: 0,
    suon: 0,
    salad: 0,
    soLuongNuoc: 0,
    thanhTien: 0,
    selectTypeValue: "",
    inputVoucherId: "",
    pendingCheckVoucher: false,
    checkResult: "",
    idLoaiNuocUong: "",
    hoTen: "",
    email: "",
    soDienThoai: "",
    diaChi: "",
    loiNhan: "",
    successMess: "",
    orderById: {},
    trangThai: "",
    showDeleteModal: false,
    deleteMess: "",
    deleteId: ""
};

const orderReducer = (state = initialState, action) => {
    switch (action.type) {
        case ORDER_FETCH_PENDING:
            state.pending = true;
            break;
        case ORDER_FETCH_SUCCESS:
            state.pending = false;
            state.orders = action.data;
            break;
        case ORDER_FETCH_ERROR:
            break;
        case SELECT_SIZE_CHANGE:
            state.selectSizeValue = action.payload;

            if (state.selectSizeValue == "S") {
                state.duongKinh = 20;
                state.suon = 2;
                state.salad = 200;
                state.soLuongNuoc = 2;
                state.thanhTien = 150000;
            }

            if (state.selectSizeValue == "M") {
                state.duongKinh = 25;
                state.suon = 4;
                state.salad = 300;
                state.soLuongNuoc = 3;
                state.thanhTien = 200000;
            }

            if (state.selectSizeValue == "L") {
                state.duongKinh = 30;
                state.suon = 8;
                state.salad = 500;
                state.soLuongNuoc = 4;
                state.thanhTien = 250000;
            }
            break;
        case SELECT_TYPE_CHANGE:
            state.selectTypeValue = action.payload;
            break;
        case INPUT_VOUCHER_ID_CHANGE:
            state.inputVoucherId = action.payload;
            break;
        case VOUCHER_FETCH_PENDING:
            state.pendingCheckVoucher = true;
            break;
        case VOUCHER_FETCH_SUCCESS:
            state.pendingCheckVoucher = false;
            if (action.data <= 0 || action.data > 100) {
                state.checkResult = "Mã voucher lỗi";
                state.inputVoucherId = "";
            } else {
                state.checkResult = "(Bạn được giảm " + action.data + "%)";

                var vTiLeGiam = (parseInt(action.data) / 100).toFixed(2);
                state.thanhTien = (state.thanhTien * (1 - vTiLeGiam));
            }
            break;
        case VOUCHER_FETCH_ERROR:
            break;
        case SELECT_DRINK_CHANGE:
            state.idLoaiNuocUong = action.payload;
            break;
        case INPUT_HOTEN_CHANGE:
            state.hoTen = action.payload;
            break;
        case INPUT_EMAIL_CHANGE:
            state.email = action.payload;
            break;
        case INPUT_SDT_CHANGE:
            state.soDienThoai = action.payload;
            break;
        case INPUT_DIA_CHI_CHANGE:
            state.diaChi = action.payload;
            break;
        case INPUT_LOI_NHAN_CHANGE:
            state.loiNhan = action.payload;
            break;
        case CREATE_DON_PENDING:
            break;
        case CREATE_DON_SUCCESS:
            state.successMess = "Tạo đơn hàng thành công!";
            break;
        case CREATE_DON_ERROR:
            break;
        case CLEAR_ORDER_DATA:
            state.selectSizeValue = "";
            state.duongKinh = 0;
            state.suon = 0;
            state.salad = 0;
            state.soLuongNuoc = 0;
            state.thanhTien = 0;
            state.selectTypeValue = "";
            state.inputVoucherId = "";
            state.idLoaiNuocUong = "";
            state.hoTen = "";
            state.email = "";
            state.soDienThoai = "";
            state.diaChi = "";
            state.loiNhan = "";
            state.checkResult = "";
            state.trangThai = "";
            state.deleteMess = "";
            state.successMess = "";
            state.deleteId = "";
            break;
        case ORDER_BY_ID_FETCH_PENDING:
            state.pending = true;
            break;
        case ORDER_BY_ID_FETCH_SUCCESS:
            state.pending = false;
            state.orderById = action.data;
            state.trangThai = action.data.trangThai;
            break;
        case ORDER_BY_ID_FETCH_ERROR:
            break;
        case SELECT_STATUS_CHANGE:
            state.trangThai = action.payload;
            break;
        case EDIT_DON_PENDING:
            break;
        case EDIT_DON_SUCCESS:
            state.successMess = "Sửa đơn hàng thành công!";
            break;
        case EDIT_DON_ERROR:
            break;
        case CLEAR_MESSAGE:
            state.successMess = "";
            break;
        case BUTTON_XOA_CLICKED:
            state.showDeleteModal = true;
            state.deleteId = action.payload;
            break;
        case ON_CLOSE_MODAL:
            state.showDeleteModal = false;
            break;
        case DELETE_DON_PENDING:
            break;
        case DELETE_DON_SUCCESS:
            state.deleteMess = "Xóa đơn hàng thành công!";
            break;
        case DELETE_DON_ERROR:
            state.deleteMess = "Đã xảy ra lỗi!";
            break;
        default:
            break;
    }

    return { ...state };
}

export default orderReducer;