export const ORDER_FETCH_PENDING = "Trạng thái đợi khi gọi API lấy danh sách pizza order";

export const  ORDER_FETCH_SUCCESS = "Trạng thái thành công khi gọi API lấy danh sách pizza order";

export const  ORDER_FETCH_ERROR = "Trạng thái lỗi khi gọi API lấy danh sách pizza order";

export const SELECT_SIZE_CHANGE = "Select kích cỡ pizza change";

export const SELECT_TYPE_CHANGE = "Select loại pizza change";

export const INPUT_VOUCHER_ID_CHANGE = "Input voucher ID change";

export const VOUCHER_FETCH_PENDING = "Trạng thái đợi khi gọi API check voucher";

export const  VOUCHER_FETCH_SUCCESS = "Trạng thái thành công khi gọi API check voucher";

export const  VOUCHER_FETCH_ERROR = "Trạng thái lỗi khi gọi API check voucher";

export const SELECT_DRINK_CHANGE = "Select loại nước uống change";

export const INPUT_HOTEN_CHANGE = "Input họ tên change";

export const INPUT_EMAIL_CHANGE = "Input email change";

export const INPUT_SDT_CHANGE = "Input số điện thoại change";

export const INPUT_DIA_CHI_CHANGE = "Input địa chỉ change";

export const INPUT_LOI_NHAN_CHANGE = "Input lời nhắn change";

export const CREATE_DON_PENDING = "Trạng thái đợi khi gọi API tạo đơn hàng";

export const  CREATE_DON_SUCCESS = "Trạng thái thành công khi gọi API tạo đơn hàng";

export const  CREATE_DON_ERROR = "Trạng thái lỗi khi gọi API tạo đơn hàng";

export const CLEAR_ORDER_DATA = "Xóa trắng các dữ liệu đơn hàng";

export const ORDER_BY_ID_FETCH_PENDING = "Trạng thái đợi khi gọi API lấy order từ id (order code)";

export const  ORDER_BY_ID_FETCH_SUCCESS = "Trạng thái thành công khi gọi API lấy order từ id (order code)";

export const  ORDER_BY_ID_FETCH_ERROR = "Trạng thái lỗi khi gọi API lấy order từ id (order code)";

export const SELECT_STATUS_CHANGE = "Select trạng thái đơn hàng change";

export const EDIT_DON_PENDING = "Trạng thái đợi khi gọi API sửa đơn hàng";

export const  EDIT_DON_SUCCESS = "Trạng thái thành công khi gọi API sửa đơn hàng";

export const  EDIT_DON_ERROR = "Trạng thái lỗi khi gọi API sửa đơn hàng";

export const CLEAR_MESSAGE = "Xóa message";

export const BUTTON_XOA_CLICKED = "Nút Xóa được ấn";

export const ON_CLOSE_MODAL = "Đóng modal delete";

export const DELETE_DON_PENDING = "Trạng thái đợi khi gọi API xóa đơn hàng";

export const  DELETE_DON_SUCCESS = "Trạng thái thành công khi gọi API xóa đơn hàng";

export const  DELETE_DON_ERROR = "Trạng thái lỗi khi gọi API xóa đơn hàng";
