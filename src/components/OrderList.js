import { Box, Button, CircularProgress, Container, Pagination, Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Typography, styled, tableCellClasses } from "@mui/material";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { btnXoaClickedAction, clearOrderDataAction, fetchOrder } from "../actions/order.action";
import usePagination from "../actions/pagination";
import { useNavigate } from "react-router-dom";
import DeleteModal from "./DeleteModal";

const StyledTableCell = styled(TableCell)(({ theme }) => ({
    [`&.${tableCellClasses.head}`]: {
        fontWeight: "bold",
        border: "1px solid rgb(203, 203, 203)"
    },
    [`&.${tableCellClasses.body}`]: {
        fontSize: 14,
        border: "1px solid rgb(203, 203, 203)"
    },
}));

const StyledTableRow = styled(TableRow)(({ theme }) => ({
    '&:nth-of-type(odd)': {
        backgroundColor: theme.palette.action.hover,
    }
}));

const OrderList = () => {
    const dispatch = useDispatch();

    const { orders, pending } = useSelector(reduxData => reduxData.orderReducer);

    useEffect(() => {
        dispatch(fetchOrder());
    }, []);

    //phân trang
    let [page, setPage] = useState(1);
    const PER_PAGE = 10;

    const count = Math.ceil(orders.length / PER_PAGE);
    const _DATA = usePagination(orders, PER_PAGE);

    //hàm thực thi khi thay đổi trang
    const handleChange = (e, p) => {
        setPage(p);
        _DATA.jump(p);
    };

    const navigate = useNavigate();
    //hàm thực thi khi nhấn nút Thêm đơn hàng
    const onBtnThemClicked = () => {
        dispatch(clearOrderDataAction());
        navigate("/create");
    }

    //hàm thực thi khi nhấn nút Sửa
    const onBtnSuaClicked = (id) => {
        dispatch(clearOrderDataAction());
        navigate("/edit/" + id);
    }

    //hàm thực thi khi nhấn nút Xóa
    const onBtnXoaClicked = (id) => {
        dispatch(btnXoaClickedAction(id));
    }

    return (
        <>
            <Container style={{ textAlign: "center" }}>
                <Typography variant="h4" style={{ margin: "2rem auto 0rem auto" }}>Danh sách đơn hàng pizza</Typography>

                <Box sx={{textAlign: "start"}}>
                    <Button variant="contained" style={{ backgroundColor: "#43a047", textTransform: "capitalize", marginBottom: "1rem" }}
                        onClick={onBtnThemClicked}
                    >Thêm đơn hàng</Button>
                </Box>

                {
                    pending ? <CircularProgress />
                        : <TableContainer component={Paper}>
                            <Table sx={{ minWidth: 700 }} aria-label="customized table">
                                <TableHead>
                                    <TableRow>
                                        <StyledTableCell>Mã đơn</StyledTableCell>
                                        <StyledTableCell>Kích cỡ</StyledTableCell>
                                        <StyledTableCell>Loại pizza</StyledTableCell>
                                        <StyledTableCell>Thành tiền</StyledTableCell>
                                        <StyledTableCell>Họ và tên</StyledTableCell>
                                        <StyledTableCell>Điện thoại</StyledTableCell>
                                        <StyledTableCell>Trạng thái</StyledTableCell>
                                        <StyledTableCell sx={{ textAlign: "center" }}>Action</StyledTableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                    {_DATA.currentData().map((e, i) => (
                                        <StyledTableRow key={i}>
                                            <StyledTableCell component="th" scope="row">
                                                {e.orderCode}
                                            </StyledTableCell>
                                            <StyledTableCell>{e.kichCo}</StyledTableCell>
                                            <StyledTableCell>{e.loaiPizza}</StyledTableCell>
                                            <StyledTableCell>{e.thanhTien}</StyledTableCell>
                                            <StyledTableCell>{e.hoTen}</StyledTableCell>
                                            <StyledTableCell>{e.soDienThoai}</StyledTableCell>
                                            <StyledTableCell>{e.trangThai}</StyledTableCell>
                                            <StyledTableCell>
                                                <Button onClick={() => onBtnSuaClicked(e.orderCode)} size="small" color="success" variant="contained" style={{ textTransform: "capitalize", display: "block", margin: "auto auto 5px auto" }}>Sửa</Button>
                                                <Button onClick={() => onBtnXoaClicked(e.id)} size="small" color="success" variant="contained" style={{ textTransform: "capitalize", display: "block", margin: "auto" }}>Xóa</Button>
                                            </StyledTableCell>
                                        </StyledTableRow>
                                    ))}
                                </TableBody>
                            </Table>
                        </TableContainer>
                }

                <Pagination count={count} variant="outlined" color="success"
                    style={{ margin: "2rem", display: "flex", justifyContent: "center" }}
                    page={page}
                    onChange={handleChange}
                />

                <DeleteModal />
            </Container>
        </>
    );
}

export default OrderList;