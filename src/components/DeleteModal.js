import { Alert, Box, Button, Modal, Typography } from "@mui/material";
import { useDispatch, useSelector } from "react-redux";
import { btnXacNhanXoaClickedAction, clearOrderDataAction, fetchOrder, onModalClose } from "../actions/order.action";

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: 'background.paper',
    boxShadow: 24,
    p: 4,
};

const DeleteModal = () => {
    const dispatch = useDispatch();

    const { showDeleteModal, deleteMess, deleteId } = useSelector(reduxData => reduxData.orderReducer);

    const handleClose = () => {
        dispatch(onModalClose());
        dispatch(clearOrderDataAction());
        dispatch(fetchOrder());
    }

    const onBtnXacNhanClicked = () => {
        dispatch(btnXacNhanXoaClickedAction(deleteId));
    };
 
    return (
        <div>
            <Modal
                open={showDeleteModal}
                onClose={handleClose}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <Box sx={style}>
                    <Typography id="modal-modal-title" variant="h6" component="h2">
                        Xác nhận
                    </Typography>
                    <Box id="modal-modal-description" sx={{ my: "1rem" }}>
                        <Typography>Bạn có chắc chắn muốn xóa đơn hàng này?</Typography>
                    </Box>

                    {
                        deleteMess ? <Alert sx={{ marginBottom: "1rem" }} 
                            severity={deleteMess == "Xóa đơn hàng thành công!" ? "success" : "error"}
                        >{deleteMess}</Alert> : <></>
                    }

                    <div style={{ margin: "1rem auto", textAlign: "end" }}>
                        <Button onClick={onBtnXacNhanClicked} color="success" variant="contained" style={{ marginRight: "1rem" }}>XÁC NHẬN</Button>
                        <Button onClick={handleClose} color="success" variant="contained">TRỞ VỀ</Button>
                    </div>
                </Box>
            </Modal>
        </div>
    );
}

export default DeleteModal;