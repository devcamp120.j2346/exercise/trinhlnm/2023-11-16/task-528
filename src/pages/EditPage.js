import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate, useParams } from "react-router-dom";
import { btnSuaDonHangClickedAction, clearMessageAction, fetchOrderById, selectStatusChangeAction } from "../actions/order.action";
import "bootstrap/dist/css/bootstrap.min.css";
import { Alert, Button, FormControl, InputLabel, MenuItem, Select } from "@mui/material";

const EditPage = () => {
    const { id } = useParams();
    const navigate = useNavigate();
    const dispatch = useDispatch();

    const { orderById, trangThai, successMess } = useSelector(reduxData => reduxData.orderReducer);

    useEffect(() => {
        dispatch(fetchOrderById(id));
    }, []);

    const handleSelectTrangThaiChange = (event) => {
        dispatch(selectStatusChangeAction(event.target.value));
    };

    const onBtnTroVeClicked = () => {
        navigate(-1, { replace: true })
    }

    const onBtnSuaDonHangClicked = () => {
        dispatch(btnSuaDonHangClickedAction(trangThai, orderById.id));
    };

    const onCloseAlertClicked = () => {
        dispatch(clearMessageAction());
    };

    return (
        <div className="container text-center">
            <h2 className="text-center" style={{ margin: "2rem" }}>Sửa đơn hàng</h2>

            <div className="row" style={{ margin: "2rem", fontSize: "18px" }}>
                <div className="col-md-6">
                    <div>Order Code: {orderById.orderCode}</div>
                    <div>Kích cỡ pizza: {orderById.kichCo}</div>
                    <div>Loại pizza: {orderById.loaiPizza}</div>
                    <div>Đường kính: {orderById.duongKinh} cm</div>
                    <div>Số lượng sườn nướng: {orderById.suon}</div>
                    <div>Salad: {orderById.salad} g</div>
                    <div>Số lượng nước uống: {orderById.soLuongNuoc}</div>
                    <div>Loại nước uống: {orderById.idLoaiNuocUong}</div>
                </div>

                <div className="col-md-6">
                    <div>Họ tên: {orderById.hoTen}</div>
                    <div>Email: {orderById.email}</div>
                    <div>Số điện thoại: {orderById.soDienThoai}</div>
                    <div>Địa chỉ: {orderById.diaChi}</div>
                    <div>Lời nhắn: {orderById.loiNhan}</div>
                    <div>Thành tiền: {orderById.thanhTien}</div>
                </div>
            </div>


            <div style={{ margin: "2rem" }}>
                <FormControl variant="standard" style={{ marginBottom: "1rem", width: "20rem" }}>
                    <InputLabel>Trạng thái</InputLabel>
                    <Select
                        onChange={handleSelectTrangThaiChange}
                        value={trangThai}
                        label="Trạng thái"
                    >
                        <MenuItem value="open">Open</MenuItem>
                        <MenuItem value="cancel">Đã hủy</MenuItem>
                        <MenuItem value="confirmed">Đã xác nhận</MenuItem>
                    </Select>
                </FormControl>
            </div>

            {
                successMess ? <Alert onClose={onCloseAlertClicked} sx={{marginBottom: "1rem", display: "inline-flex"}} severity="success">{successMess}</Alert> : <></>
            }

            <div style={{ marginBottom: "4rem" }}>
                <Button onClick={onBtnSuaDonHangClicked} color="success" variant="contained" style={{ marginRight: "1rem" }}>XÁC NHẬN</Button>
                <Button onClick={onBtnTroVeClicked} color="success" variant="contained">TRỞ VỀ</Button>
            </div>
        </div>
    );
}

export default EditPage;