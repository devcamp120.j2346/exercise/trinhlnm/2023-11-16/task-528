import { Alert, Button, FormControl, InputLabel, MenuItem, Select, TextField } from "@mui/material";
import "../App.css";
import "bootstrap/dist/css/bootstrap.min.css";
import { useDispatch, useSelector } from "react-redux";
import { btnCheckVoucherClickedAction, btnTaoDonHangClickedAction, clearMessageAction, inputDiaChiChangeAction, inputEmailChangeAction, inputHotenChangeAction, inputLoiNhanChangeAction, inputSdtChangeAction, inputVoucherIDChangeAction, selectDrinkChangeAction, selectSizeChangeAction, selectTypeChangeAction } from "../actions/order.action";
import { useNavigate } from "react-router-dom";

const CreatePage = () => {
    const dispatch = useDispatch();
    const navigate = useNavigate();

    const { 
        selectSizeValue, duongKinh, suon, salad, soLuongNuoc, thanhTien,
        selectTypeValue, inputVoucherId, pendingCheckVoucher, checkResult,
        idLoaiNuocUong, hoTen, email, soDienThoai, diaChi, loiNhan, successMess
    } = useSelector((reduxData) => {
        return reduxData.orderReducer;
    });

    const handleSelectSizeChange = (event) => {
        dispatch(selectSizeChangeAction(event.target.value));
    };

    const handleSelectTypeChange = (event) => {
        dispatch(selectTypeChangeAction(event.target.value));
    };

    const handleInputVoucherIdChange = (event) => {
        dispatch(inputVoucherIDChangeAction(event.target.value));
    };

    const onBtnCheckVoucherClicked = () => {
        dispatch(btnCheckVoucherClickedAction(inputVoucherId));
    };

    const handleSelectDrinkChange = (event) => {
        dispatch(selectDrinkChangeAction(event.target.value));
    };

    const handleInputHotenChange = (event) => {
        dispatch(inputHotenChangeAction(event.target.value));
    };

    const handleInputEmailChange = (event) => {
        dispatch(inputEmailChangeAction(event.target.value));
    };

    const handleInputSdtChange = (event) => {
        dispatch(inputSdtChangeAction(event.target.value));
    };

    const handleInputDiaChiChange = (event) => {
        dispatch(inputDiaChiChangeAction(event.target.value));
    };

    const handleInputLoiNhanChange = (event) => {
        dispatch(inputLoiNhanChangeAction(event.target.value));
    };

    const onBtnTaoDonHangClicked = () => {
        dispatch(btnTaoDonHangClickedAction(
            selectSizeValue, duongKinh, suon, salad,
            selectTypeValue, inputVoucherId, idLoaiNuocUong, soLuongNuoc, 
            hoTen, thanhTien, email, soDienThoai, diaChi, loiNhan
        ));
    };

    const onBtnTroVeClicked = () => {
        navigate(-1, { replace: true })
    }

    const onCloseAlertClicked = () => {
        dispatch(clearMessageAction());
    };

    return (
        <div className="container">
            <h2 className="text-center" style={{ margin: "2rem" }}>Thêm đơn hàng</h2>
            <div className="row">
                <div className="col-md-6" style={{ paddingRight: "2rem" }}>
                    <FormControl fullWidth variant="standard" style={{ marginBottom: "1rem" }}>
                        <InputLabel>Kích cỡ</InputLabel>
                        <Select
                            onChange={handleSelectSizeChange}
                            label="Kích cỡ"
                            value={selectSizeValue}
                        >
                            <MenuItem value="S">S</MenuItem>
                            <MenuItem value="M">M</MenuItem>
                            <MenuItem value="L">L</MenuItem>
                        </Select>
                    </FormControl>

                    <div>Đường kính: {duongKinh} cm</div>
                    <div>Số lượng sườn nướng: {suon}</div>
                    <div>Salad: {salad} g</div>
                    <div>Số lượng nước uống: {soLuongNuoc}</div>
                    <div style={{ marginBottom: "1.5rem" }} >Thành tiền: {thanhTien} VNĐ</div>

                    <FormControl fullWidth variant="standard" style={{ marginBottom: "1rem" }}>
                        <InputLabel>Loại pizza</InputLabel>
                        <Select
                            onChange={handleSelectTypeChange}
                            value={selectTypeValue}
                            label="Loại pizza"
                        >
                            <MenuItem value="Seafood">Hải sản</MenuItem>
                            <MenuItem value="Hawaii">Hawaii</MenuItem>
                            <MenuItem value="Bacon">Thịt hun khói</MenuItem>
                        </Select>
                    </FormControl>

                    <div style={{ marginBottom: "1rem", width: "100%" }}>
                        <div style={{ display: "flex", justifyContent: "space-between", alignItems: "center" }}>
                            <TextField onChange={handleInputVoucherIdChange} id="standard-basic" label="Voucher ID" variant="standard" style={{ width: "80%" }} value={inputVoucherId} />
                            <Button onClick={onBtnCheckVoucherClicked} size="small" variant="outlined" color="success" style={{ textTransform: "capitalize", width: "10%", marginTop: "auto", fontWeight: "600" }}>Check</Button>
                        </div>
                        <div style={{fontSize: "12px", fontStyle: "italic"}}>
                            {
                                pendingCheckVoucher ? "..." : checkResult
                            }
                        </div>
                    </div>

                    <FormControl fullWidth variant="standard">
                        <InputLabel>Loại nước uống</InputLabel>
                        <Select
                            onChange={handleSelectDrinkChange}
                            value={idLoaiNuocUong}
                            label="Loại nước uống"
                        >
                            <MenuItem value="TRATAC">Trà tắc</MenuItem>
                            <MenuItem value="COCA">Cocacola</MenuItem>
                            <MenuItem value="PEPSI">Pepsi</MenuItem>
                            <MenuItem value="LAVIE">Lavie</MenuItem>
                            <MenuItem value="TRASUA">Trà sữa trân châu</MenuItem>
                            <MenuItem value="FANTA">Fanta</MenuItem>
                        </Select>
                    </FormControl>
                </div>
                <div className="col-md-6" style={{ paddingRight: "2rem" }}>
                    <TextField onChange={handleInputHotenChange} value={hoTen} style={{ marginBottom: "1rem" }} fullWidth id="standard-basic" label="Họ tên" variant="standard" />
                    <TextField onChange={handleInputEmailChange} value={email} style={{ marginBottom: "1rem" }} fullWidth id="standard-basic" label="Email" variant="standard" />
                    <TextField onChange={handleInputSdtChange} value={soDienThoai} style={{ marginBottom: "1rem" }} fullWidth id="standard-basic" label="Số điện thoại" variant="standard" />
                    <TextField onChange={handleInputDiaChiChange} value={diaChi} style={{ marginBottom: "1rem" }} fullWidth id="standard-basic" label="Địa chỉ" variant="standard" />
                    <TextField onChange={handleInputLoiNhanChange} value={loiNhan} fullWidth id="standard-basic" label="Lời nhắn" variant="standard" />

                    {
                        successMess ? <Alert onClose={onCloseAlertClicked} sx={{marginTop: "2rem"}} severity="success">{successMess}</Alert> : <></>
                    }
                </div>
            </div>

            <div style={{ float: "right", marginBottom: "3rem" }}>
                <Button onClick={onBtnTaoDonHangClicked} color="success" variant="contained" style={{ marginRight: "1rem" }}>TẠO ĐƠN HÀNG</Button>
                <Button onClick={onBtnTroVeClicked} color="success" variant="contained">TRỞ VỀ</Button>
            </div>
        </div>
    );
}

export default CreatePage;