import CreatePage from "./pages/CreatePage";
import EditPage from "./pages/EditPage";
import ListPage from "./pages/ListPage";

const routes = [
    {path: "/", element: <ListPage/>},
    {path: "/create", element: <CreatePage/>},
    {path: "/edit/:id", element: <EditPage/>},
];

export default routes;